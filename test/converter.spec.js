// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to hex conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255.,0,0);
            const greenHex = converter.rgbToHex(0,255,0);
            const blueHex = converter.rgbToHex(0,0,255);

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    });
});